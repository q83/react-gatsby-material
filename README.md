# React (Gatsby, Material)

# Requirements:

* Over 600MB free to install the repository.
* NPM, install this link:
[Node and NPM Install](https://nodejs.org/en/)
* Gatsby, install using the below command:

`npm install --global gatsby-cli`

# Running on your computer

* Clone the repository using this command:

`gatsby new gatsby-test https://github.com/Vagr9K/gatsby-material-starter`

* Go inside the "react-gatsby-material" folder and install dependencies:

`npm install`

* Use the below command inside the project directory:

`gatsby develop`

* Code changes will be automatically compiled when you save the file.

# Challenge 1

* Home Page: Render articles in 2 columns on desktop, 1 column on mobile.
* Verify that it works in Chrome and Firefox

![challenge_1]( https://media.q-83.com/dummy_service/challenge_1.png)

# Challenge 2

* Add a new datasource using the below URL:

`https://9ss7bxey8k.execute-api.ap-southeast-2.amazonaws.com/default/dummy_service`

* You'll need to install a network api like "axios" to run the remote request.

![challenge_2](https://media.q-83.com/dummy_service/challenge_3.png)

# Challenge 3

* Display a "Post" component when you click on the blog post.

![challenge_3](https://media.q-83.com/dummy_service/challenge_2.png)

# Final challenges

* Plenty of clean up to do, try getting the "next" and "previous" buttons on the Post component to work.
* Any other enhancements you can think of.